﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Cirsa.Framework;
using Cirsa.Framework.Logger;
using Cirsa.Gaming.Games.Interfaces;
using Cirsa.Gaming.Games.Model;
using Cirsa.Gaming.Games.Modules;
using log4net;

namespace Cirsa.Gaming.Games.Api.Controllers
{
    public class GamesController : ApiController
    {

        /// <summary>
        /// The log
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(GamesController));

        /// <summary>
        /// The handle
        /// </summary>
        private MessageProcessHandle _handle;
        /// <summary>
        /// The track
        /// </summary>
        private ITrack _track;

        /// <summary>
        /// Initializes a new instance of the <see cref="GamesController"/> class.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <param name="track">The track.</param>
        public GamesController(MessageProcessHandle handle, ITrack track)
        {
            _track = track;
            _handle = handle;
        }

        /// <summary>
        /// Plays the specified game identifier.
        /// </summary>
        /// <param name="gameId">The game identifier.</param>
        /// <returns></returns>
        [Route("api/games/{gameId}/play")]
        [HttpPost]
        public async Task<IHttpActionResult> Play(string gameId)
        {
            _log.Debug($"Play Action called... GameId:{gameId}");
            try
            {
                var gameMessage = new PlayGameMessageAction
                {
                    GameId = gameId
                };

                CounterPerformanceTime.StartCounter(gameMessage.Id);

                //Prepare the domain entity to execute...
                var result = await _handle.HandleMessage(gameMessage);

                //Loging the time
                var time = CounterPerformanceTime.StopCounter(gameMessage.Id);
                _track.TrackGameAndTime(gameId, time);

                _log.Debug($"End Play Action called GameId:{gameId}");
                return Ok(Json(result).Content);
            }
            catch (ValidationException validationException)
            {
                _log.Error($"Error in validation. Exception:{validationException.Message}");
                return BadRequest(validationException.Message);
            }
            catch (Exception exception)
            {
                _log.Error($"Exception: {exception.Message}");
                return BadRequest(exception.Message);
            }
        }

    }
}
