﻿using Cirsa.Gaming.Games.Interfaces;
using Cirsa.Gaming.Games.Modules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cirsa.Gaming.Games.U.Test
{
    [TestClass]
    public class ValidationTest
    {
        [TestMethod]
        public void ValiadtionOkTest()
        {

            IValidation validation = new Validation();

            var gameOk = "eltesorodejava";

            Assert.IsTrue(validation.IsValid(gameOk));
        }

        [TestMethod]
        public void ValiadtionNotOkTest()
        {

            IValidation validation = new Validation();

            var gameOk = "eltesorodejavaaaaaa";

            Assert.IsFalse(validation.IsValid(gameOk));
        }

        [TestMethod]
        public void ValiadtionNotOkTotallydifferntTest()
        {

            IValidation validation = new Validation();

            var gameOk = "HELlO!!!";

            Assert.IsFalse(validation.IsValid(gameOk));
        }


    }
}
