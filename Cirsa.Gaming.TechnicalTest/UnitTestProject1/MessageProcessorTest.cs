﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cirsa.Framework;
using Cirsa.Gaming.Games.Interfaces;
using Cirsa.Gaming.Games.Model;
using Cirsa.Gaming.Games.Modules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cirsa.Gaming.Games.U.Test
{
    [TestClass]
    public class MessageProcessorTest
    {
        [TestMethod]
        public void MessageProcessOk()
        {
            string game = "eltesorodejava";

            IValidation validation = new Validation();
            MessageProcessHandle message = new MessageProcessHandle(validation);

            PlayGameMessageAction action = new PlayGameMessageAction() {GameId =  game};
            var t = message.HandleMessage(action);

            Assert.IsTrue(t.Result.Any());
            Assert.AreEqual(t.Result.Count, 5);
        }


        [TestMethod]
        public void MessageProcessNotOk()
        {
            string game = "eltesorodejavaaaaaaaaa";

            IValidation validation = new Validation();
            MessageProcessHandle message = new MessageProcessHandle(validation);
            PlayGameMessageAction action = new PlayGameMessageAction() { GameId = game };

            bool exception = false;
            try
            {
                var res = message.HandleMessage(action).Wait(100);

            }
            catch (Exception s)
            {
                    exception = true; 

            }
            Assert.IsTrue(exception);

        }

    }
}
