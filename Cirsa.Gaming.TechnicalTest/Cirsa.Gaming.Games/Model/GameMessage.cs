﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cirsa.Gaming.Games.Model
{
    /// <summary>
    /// Domain class of play game action 
    /// </summary>
    public class PlayGameMessageAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayGameMessageAction"/> class.
        /// </summary>
        public PlayGameMessageAction()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; }

        /// <summary>
        /// Gets or sets the game identifier.
        /// </summary>
        /// <value>
        /// The game identifier.
        /// </value>
        public string GameId { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format($"Id:<{Id}>  GameId:<{GameId}>");
        }
    }


}
