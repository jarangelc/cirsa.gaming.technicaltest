﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cirsa.Gaming.Games.Interfaces
{
    /// <summary>
    /// IHandleMessage interface to implement different times of domain entites and results.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public interface IHandleMessage<T,TResult> where T : class 
    {
        /// <summary>
        /// Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        Task<TResult> HandleMessage(T message);
    }

    /// <summary>
    /// Generic IHandle interface to get all the implementitions that implements this interface (building in the container)
    /// </summary>
    public interface IHandle
    {
        
    }

}
