﻿namespace Cirsa.Gaming.Games.Interfaces
{
    /// <summary>
    /// IValidation interface to implement
    /// </summary>
    public interface IValidation
    {
        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>
        ///   <c>true</c> if the specified path is valid; otherwise, <c>false</c>.
        /// </returns>
        bool IsValid(string path);
    }
}