﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cirsa.Gaming.Games.Model;

namespace Cirsa.Gaming.Games.Helpers
{
    /// <summary>
    /// Static helper to convert into a CharIndex object
    /// </summary>
    public static class CharIndexHelper
    {
        /// <summary>
        /// Parses the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="find">The find.</param>
        /// <returns></returns>
        public static List<CharIndex> ParseMessage(string message, char find)
        {
            List<CharIndex> listcharIndex = new List<CharIndex>();

            var arrayGame = message.ToArray();

            for (int ipos = 0; ipos < arrayGame.Length; ipos++)
            {
                if (arrayGame[ipos] == find)
                {
                    break;
                }

                var index = new CharIndex()
                {
                    Index = ipos,
                    Char = arrayGame[ipos]
                };

                listcharIndex.Add(index);
            }


            return listcharIndex;

        }
    }
}
