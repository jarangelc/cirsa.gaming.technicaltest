﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Cirsa.Framework;
using Cirsa.Framework.Logger;
using Cirsa.Gaming.Games.Helpers;
using Cirsa.Gaming.Games.Interfaces;
using Cirsa.Gaming.Games.Model;

using log4net;
using log4net.Core;

namespace Cirsa.Gaming.Games.Modules
{

    /// <summary>
    /// The Message Process Handle of PlayGameMessageAction
    /// </summary>
    /// <seealso cref="Cirsa.Gaming.Games.Interfaces.IHandleMessage{Cirsa.Gaming.Games.Model.PlayGameMessageAction, System.Collections.Generic.List{Cirsa.Gaming.Games.Model.CharIndex}}" />
    /// <seealso cref="Cirsa.Gaming.Games.Interfaces.IHandle" />
    public class MessageProcessHandle : IHandleMessage<PlayGameMessageAction, List<CharIndex>>, IHandle
    {

        /// <summary>
        /// The log
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(MessageProcessHandle));

        /// <summary>
        /// The validation
        /// </summary>
        private IValidation _validation;

        /// <summary>
        /// The track
        /// </summary>
        private ITrack _track;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageProcessHandle"/> class.
        /// </summary>
        /// <param name="validation">The validation.</param>
        /// <param name="track">The track.</param>
        public MessageProcessHandle(IValidation validation, ITrack track)
        {
            _validation = validation;
            _track = track;
        }

        /// <summary>
        /// Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        /// <exception cref="ValidationException"></exception>
        public async Task<List<CharIndex>> HandleMessage(PlayGameMessageAction message)
        {
            _log.Info($"Start Handle Message:{message}");

            if (!_validation.IsValid(message.GameId))
            {
                _track.TrackError(message.GameId, "Invalid validation");
                throw new ValidationException($"Invalid message: {message}");
            }

            //Default 500 milis
            var result = CharIndexHelper.ParseMessage(message.GameId, 'o');

            Thread.Sleep(500);

            _log.Info($"End Handle Message:{message}");

            return result;

        }

    }

}
