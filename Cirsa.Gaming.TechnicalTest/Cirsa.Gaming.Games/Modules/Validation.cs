﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Cirsa.Framework;
using Cirsa.Framework.Logger;
using Cirsa.Gaming.Games.Interfaces;
using log4net;
using log4net.Repository.Hierarchy;

namespace Cirsa.Gaming.Games.Modules
{
    public class Validation: IValidation
    {
        /// <summary>
        /// The log
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(Validation));

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <param name="gameId">The game identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified game identifier is valid; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValid(string gameId)
        {
            string game = ConfigurationManager.AppSettings["Game"];

            if (gameId != game)
            {

                _log.Error($"GameId:{gameId} is invalid. ExpectedValue:{game}");
                return false;
            }

            _log.Debug($"GameId:{gameId} is valid. ExpectedValue:{game}");
            return true;

        } 

    }
}
