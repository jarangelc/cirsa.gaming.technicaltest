﻿using Owin;
using System.Web.Http;

namespace Cirsa.Gaming.Server.WinService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration HttpConfiguration = new HttpConfiguration();
            HttpConfiguration.MapHttpAttributeRoutes();

            var gamesBootstrapper = new GamesBootstrapper();
            gamesBootstrapper.Run(HttpConfiguration);

            app.UseWebApi(HttpConfiguration);
        }
    }
}
