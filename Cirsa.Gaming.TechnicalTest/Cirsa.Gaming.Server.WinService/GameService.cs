﻿using Microsoft.Owin.Hosting;
using System;
using System.Diagnostics;

namespace Cirsa.Gaming.Server.WinService
{
    public class GameService
    {
        const string ServerAddress = "http://localhost:8888";
        private IDisposable _app;

        public void Start()
        {
            _app = WebApp.Start<Startup>(new StartOptions(ServerAddress));

            if (_app != null)
            {
                Debug.WriteLine(string.Format("Server listening at {0}", ServerAddress));
            }
        }

        public void Stop()
        {
            if (_app != null)
            {
                _app.Dispose();
            }
        }
    }
}
