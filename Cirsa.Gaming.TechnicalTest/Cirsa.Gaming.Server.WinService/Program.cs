﻿using log4net;

using Topshelf;

namespace Cirsa.Gaming.Server.WinService
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                log4net.Config.XmlConfigurator.Configure();

                x.Service<GameService>(s =>
                {
                   
                    s.ConstructUsing(name => new GameService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                   
                });

                
                x.RunAsLocalSystem();

                x.SetDescription("Web server for slot games");
                x.SetDisplayName("Games Web server");
                x.SetServiceName("GamesWebServer");
            });
        }
    }
}
