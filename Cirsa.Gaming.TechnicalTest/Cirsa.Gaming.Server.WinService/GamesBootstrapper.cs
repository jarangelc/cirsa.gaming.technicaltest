﻿using Cirsa.Gaming.Games.Api.Controllers;
using LightInject;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;

using Cirsa.Framework.Logger;
using Cirsa.Gaming.Games.Interfaces;
using Cirsa.Gaming.Games.Model;
using Cirsa.Gaming.Games.Modules;



namespace Cirsa.Gaming.Server.WinService
{
    class GamesBootstrapper
    {
        ServiceContainer _serviceContainer;

        public GamesBootstrapper()
        {
            CreateContainer();
        }

        public void Run(HttpConfiguration config)
        {
            RegisterWebApi(config);
            RegisterServices();
        }

        protected void CreateContainer()
        {
            _serviceContainer = new ServiceContainer();
        }

        protected void RegisterWebApi(HttpConfiguration config)
        {
            _serviceContainer.RegisterApiControllers(GetApiAssemblies().ToArray());
            _serviceContainer.EnableWebApi(config);
            _serviceContainer.ScopeManagerProvider = new PerLogicalCallContextScopeManagerProvider();

        }

        protected void RegisterServices()
        {
            //_serviceContainer.RegisterInstance(typeof(IHandleMessage<,>), typeof(MessageProcessHandle));
            _serviceContainer.Register<MessageProcessHandle>();
            _serviceContainer.Register<IValidation, Validation>();
            _serviceContainer.Register<ITrack, Tracking>();

            
        }

        protected ICollection<Assembly> GetApiAssemblies()
        {
            ICollection<Assembly> assemblies = new List<Assembly>();
            assemblies.Add(typeof(GamesController).Assembly);
            return assemblies;
        }
    }
}
