﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Cirsa.Framework.Logger
{
    public class Tracking : ITrack
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(Tracking));

        public void TrackGameAndTime(string game, long time )
        {
            string path = @"c:\LogFiles\TrackTimes.txt";

            using (StreamWriter w = File.AppendText(path))
            {
                w.WriteLine(string.Format($"Timestamp: <{DateTime.Now}> Game: <{game}> TimeOfProcess:<{time}>"));
            }

            _logger.Info($"Timestamp: <{DateTime.Now}> Game: <{game}> TimeOfProcess:<{time}>");
        }

        public void TrackError(string game, string error)
        {
            string path = @"c:\LogFiles\TrackErrors.txt";
            using (StreamWriter w = File.AppendText(path))
            {
                w.WriteLine(string.Format($"Timestamp: <{DateTime.Now}> Game: <{game}> Error:<{error}>"));
            }

            _logger.Error($"Timestamp: <{DateTime.Now}> Game: <{game}> Error:<{error}>");
        }

    }
}
