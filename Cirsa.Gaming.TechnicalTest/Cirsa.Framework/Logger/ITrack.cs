﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cirsa.Framework.Logger
{
    public interface ITrack
    {
        void TrackGameAndTime(string game, long time);
        void TrackError(string game, string error);
    }
}
