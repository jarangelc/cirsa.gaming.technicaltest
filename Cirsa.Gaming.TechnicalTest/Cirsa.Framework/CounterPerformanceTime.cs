﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cirsa.Framework
{
    public static class CounterPerformanceTime
    {

        public static ConcurrentDictionary<Guid, Stopwatch> Counterperformance = new ConcurrentDictionary<Guid, Stopwatch>();
       
        public static void StartCounter(Guid guid)
        {
            Stopwatch stopwatch;
            if (!Counterperformance.TryGetValue(guid, out stopwatch))
            {
                stopwatch = new Stopwatch();
                if (!Counterperformance.TryAdd(guid, stopwatch))
                {
                    return;
                }
            }

            Counterperformance[guid].Start();
        }

        public static long StopCounter(Guid guid)
        {
            Stopwatch stopwatch;
            if (!Counterperformance.TryGetValue(guid, out stopwatch))
            {
                return 0;
            }
          
            Counterperformance.TryRemove(guid, out stopwatch);
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }


    }
}
